name = "Louise"
weather = "sunny"

responses = {
    "what's your name ?": [
        "My name is {0}".format(name),
        "They call me {0}".format(name),
        "I am {0}, nice to meet you :)".format(name)
    ],
    "what's today weather?": [
        "the weather is {0}".format(weather)
    ],
    "default": [
        "tell me more !",
        "why do you think like that ?",
        "I think so...",
        "Well I am just a bot and nothing else..."
    ]
}


import random

def respond(message):
    if message in responses:
        return random.choice(responses[message])
    else:
        return random.choice(responses["default"])


# respond("what's your name ?")
