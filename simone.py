import sqlite3

tests = [("no I don't want to be in the south", {'south': False}), ('no it should be in the south', {'south': True}), ('no in the south not the north', {'south': True, 'north': False}), ('not north', {'north': False})]

# conn = sqlite3.connect('hotels.db')

# c = conn.cursor()

# area, price = "south", "hi"
# t = (area, price)

# c.execute('SELECT * FROM hotels WHERE area=? AND price=?', t)

# print(c.fetchall())


def find_hotels(params, neg_params):

    query = 'SELECT * FROM hotels'

    if len(params) > 0:
        filters = ["{}=?".format(k) for k in params]
        query += " WHERE " + " and ".join(filters)
        
    t = tuple(params.values())
    
    conn = sqlite3.connect("hotels.db")
   
    c = conn.cursor()
    
    c.execute(query, t)
    
    c.fetchall()
    
    
# exemple = {"area": "south", "price": "lo"}

# print(find_hotels(exemple))

# def respond(message):
    # entities = interpreter.parse(message)["entities"]
    
    # params = {}
    
    # for ent in entities:
        # params[ent["entity"]] = str(ent["value"])
        
    
    # results = find_hotels(params)
    
    # names = [r[0] for r in results]
    # n = min(len(results),3)
    
    # return responses[n].format(*names)
    
# print(respond("I want an expensive hotel in the south of town"))


def negated_ents(phrase):
    ents = [e for e in ["south", "north"] if e in phrase]
    
    ends = sorted([phrase.index(e) + len(e) for e in ents])
    
    chunks = []
    
    start = 0
    for end in ends:
        chunks.append(phrase[start:end])
        start = end
    result = {}
    
    for chunk in chunks:
        for ent in ents:
            if ent in chunk:
                
                if "not" in chunk or "n't" in chunk:
                    result[ent] = False
                else:
                    result[ent] = True
    return result  


for test in tests:
    print(negated_ents(test[0]) == test[1])



def respond(message, params, neg_params):
    entities = interpreter.parse(message)["entities"]
    ent_vals = [e["value"] for e in entities]

    negated = negated_ents(message, ent_vals)
    for ent in entities:
        if ent["value"] in negated and negated[ent["value"]]:
            neg_params[ent["entity"]] = str(ent["value"])
        else:
            params[ent["entity"]] = str(ent["value"])

    results = find_hotels(params, neg_params)
    names = [r[0] for r in results]
    n = min(len(results),3)
   
    return responses[n].format(*names), params, neg_params


params = {}
neg_params = {}

for message in ["I want a cheap hotel", "but not in the north of town"]:
    print("USER: {}".format(message))
    response, params, neg_params = respond(message, params, neg_params)
    print("BOT: {}".format(response))
    
    

        



