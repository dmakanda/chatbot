import random
import re

name = "Louise"
weather = "sunny"
bot_template = name + " the BOT : {0}"
user_template = "USER : {0}"

keywords = {
    'goodbye': [
        'bye', 'farewell'
    ], 
    'thankyou': [
        'thank', 'thx'
    ], 
    'greet': [
        'hello', 'hi', 'hey'
    ]
}

patterns = {}

for intent, keys in keywords.items():
    # Create regular expressions and compile them into pattern objects
    patterns[intent] = re.compile('|'.join(keys))

rules = {
    'I want (.*)': [
        'What would it mean if you got {0}', 
        'Why do you want {0}', 
        "What's stopping you from getting {0}"
    ], 
    'do you remember (.*)': [
        'Did you think I would forget {0}', 
        "Why haven't you been able to forget {0}", 
        'What about {0}', 'Yes .. and?'
    ], 
    'if (.*)': [
        "Do you really think it's likely that {0}",
        'Do you wish that {0}', 
        'What do you think about {0}', 'Really--if {0}'
    ], 
    'do you think (.*)': [
        'if {0}? Absolutely.', 
        'No chance'
    ]
}

responses = {
  "what's your name?": [
      "my name is {0}".format(name),
      "they call me {0}".format(name),
      "I am {0}".format(name)
    ],
  "what's today's weather?": [
      "the weather is {0}".format(weather),
      "it's {0} today".format(weather)
    ],
  "default": [
      "Can I help you ?",
      "I am a just a bot nothing else..."
    ],
    'goodbye': 'goodbye for now', 
    'thankyou': 'you are very welcome', 
    "default": "okay...",
    'greet': 'Hello you! :)'
}


def find_name(message):
    name = None
    name_keyword = re.compile("name|call")
    name_pattern = re.compile('[A-Z]{1}[a-z]*')
    if name_keyword.search(message):
        name_words = name_pattern.findall(message)
        if len(name_words) > 0:
            name = ' '.join(name_words)
    return name


# Define replace_pronouns()
def replace_pronouns(message):

    message = message.lower()
    if 'me' in message:
        return re.sub('me', 'you', message)
    if 'my' in message:
        return re.sub('my', 'your', message)
    if 'your' in message:
        return re.sub('your', 'my', message)
    if 'you' in message:
        return re.sub('you', 'me', message)

    return message


# print(replace_pronouns("my last birthday"))
# print(replace_pronouns("when you went to Florida"))
# print(replace_pronouns("I had my own castle"))

# Define match_rule()
def match_rule(rules, message):
    response, phrase = "default", None
    
    # Iterate over the rules dictionary
    for pattern, responses in rules.items():
        # Create a match object
        match = re.search(pattern, message)
        if match is not None:
            response = random.choice(responses)
            if '{0}' in response:
                phrase = match.group(1)
    return response, phrase

# Test match_rule
# print(match_rule(rules, "do you remember your last birthday"))

def match_intent(message):
    matched_intent = None
    for intent, pattern in patterns.items():
        # Check if the pattern occurs in the message 
        if pattern.search(message):
            matched_intent = intent
    return matched_intent


def respond(message):
    intent = match_intent(message)
    key = "default"
    if intent in responses:
        key = intent
    return responses[key]
    
    response, phrase = match_rule(rules, message)
    if '{0}' in response:
        phrase = replace_pronouns(phrase)
        response = response.format(phrase)
        
    return response
    
    name = find_name(message)
    if name is None:
        return "Hi there!"
    else:
        return "Hello, {0}!".format(name)
    
    if message in responses:
        # bot_message = "I can hear you! You said: " + message
        bot_message = random.choice(responses[message])
    else:
        bot_message = random.choice(responses["default"])
        
    # Return the result
    return bot_message

def send_message(message):
    # Print user_template including the user_message
    print(user_template.format(message))
    # Get the bot's response to the message
    response = respond(message)
    # Print the bot template including the bot's response.
    print(bot_template.format(response))

# Send a message to the bot
send_message("hi")
send_message("what's my birthday")
send_message("bye byeee")
send_message("call me Délita")
