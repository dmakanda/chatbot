from sklearn.svm import SVC

clf = SVC()

clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)

n_correct = 0
for i in range(len(y_test)):
    if y_pred[i] == y_test[i]:
        n_correct += 1

print("Predicted {0} correctly out of {1} test examples".format(n_correct, len(y_test)))
