include_entities = ['DATE', 'ORG', 'PERSON']

def extract_entities(message):
    
    ents = dict.fromkeys(include_entities)
    
    doc = nlp(message)
    for ent in doc.ents:
        if ent.label_ in include_entities:
            
            ents[ent.label_] = ent.text
    return ents
    
    

doc = nlp("let's see that jacket in red and some blue jeans")

def find_parent_item(word):
    
    for parent in word.ancestors:
        
        if entity_type(parent) == "item":
            return parent.text
    return None


def assign_colors(doc):
    
    for word in doc:
        
        if entity_type(word) == "color":
            
            item =  find_parent_item(word)
            print("item: {0} has color : {1}".format(item, word))


assign_colors(doc)



print(extract_entities('friends called Mary who have worked at Google since 2010'))
print(extract_entities('people who graduated from MIT in 1999'))
