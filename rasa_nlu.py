from rasa_nlu.converters import load_data
from rasa_nlu.config import RasaNLUConfig
from rasa_nlu.model import Trainer

args = {"pipeline": "spacy_sklearn"}

pipeline = [
    "nlp_spacy",
    "tokenizer_spacy",
    "ner_crf"
]

# config = RasaNLUConfig(cmdline_args=args)
config = RasaNLUConfig(cmdline_args={"pipeline": pipeline})

trainer = Trainer(config)

training_data = load_data("./training_data.json")

interpreter = trainer.train(training_data)


print(interpreter.parse("I'm looking for a Mexican restaurant in the North of town"))
print(interpreter.parse("show me Chinese food in the centre of town"))
print(interpreter.parse("I want an Indian restaurant in the west"))
print(interpreter.parse("are there any good pizza places in the center?"))
